import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/Model/todos';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  title ="My Todos List"
  todos!: Todo[];
  localItem :string;
  constructor() {
    this.localItem= localStorage.getItem("todos") as string;
    if(this.localItem == null)
    {
      this.todos =[];
    }
    else
    {
      this.todos = JSON.parse(this.localItem);
      console.log("todoslist", this.todos)
    }
    // setTimeout(()=>{  
    //   this.title ="Title changed"
    // },2000);
   // this.todos = localStorage.getItem("todos");
  }
    
   
  ngOnInit(): void {
  }

  deleteTodo(todo:Todo)
  {
    const index = this.todos?.indexOf(todo) as number;
    this.todos?.splice(index,1);
    console.log(todo);
    localStorage.setItem("todos",JSON.stringify(this.todos))
  }

  todoAdd(todo:Todo)
  {
    this.todos?.push(todo);
    localStorage.setItem("todos",JSON.stringify(this.todos))
  }

  toggleTodos(todo:Todo)
  {
    const index = this.todos?.indexOf(todo) as number;
    this.todos[index].active = !this.todos[index].active;
    localStorage.setItem("todos",JSON.stringify(this.todos))
    console.log("Toggle", todo)
  }
}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Todo } from 'src/app/Model/todos';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {

  title?:string;
  desc?:string;
  todos: Todo[] | undefined;
  @Output() addTodo : EventEmitter<Todo> = new EventEmitter();
  constructor(public route:Router) {
    this.todos = JSON.parse(localStorage.getItem("todos") as string);
   }

  ngOnInit(): void {
  }

  onSubmit()
  {
    console.log(this.title,this.desc)
    const todo =
      {
        sno:8,
        title:this.title,
        desc:this.desc,
        active:true
      };
      this.todos?.push(todo);
      localStorage.setItem("todos",JSON.stringify(this.todos));
      this.title =""
      this.desc =""
      this.route.navigate(["/"])
  }
}
